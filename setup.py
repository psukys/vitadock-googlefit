from setuptools import setup, find_packages
from os import path
from io import open

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

# Arguments marked as "Required" below must be included for upload to PyPI.
# Fields marked as "Optional" may be commented out.

setup(
    name='vitadock-googlefit',
    version='1.0.0',
    description='Vitadock exporter to Google Fit',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/psukys/vitadock-googlefit',
    author='Paulius Sukys',
    author_email='paulius@sukys.eu',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Libraries',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
    ],
    keywords='vitadock,medisana,google,googlefit',
    packages=find_packages(exclude=['tests']),
    install_requires=['google-api-python-client'],
    extras_require={  # Optional
        'dev': ['check-manifest'],
        'test': ['coverage'],
    },
    entry_points={  # Optional
        'console_scripts': [
            'vitadock2googlefit=app.main:main',
        ],
    },
)
