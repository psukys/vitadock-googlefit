# Vitadock to Google Fit synchronizer

[Vitadock](https://cloud.vitadock.com) is a cloud platform for viewing and managing vitality based data, which is primarily used for [Medisana](https://www.medisana.com) products.

By using [Vitadock's API](https://github.com/Medisana/vitadock-api), it is possible to export their data to other services, in this case - Google Fit.
